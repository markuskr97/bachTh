#!/bin/bash
for file in /home/markus/Documents/bachTh/Sysmon/chosen-use-cases/sigma/*
do
    name=$(basename "$file" ".yml")
    /home/markus/sigma/tools/sigmac --target splunk $file > "/home/markus/Documents/bachTh/Sysmon/chosen-use-cases/splunk/splunk.$name"
done
