defaultParams = {}
defaultParams["DestinationPortName"]=""
defaultParams["Initiated"]="true"
defaultParams["SourceIsIpv6"]="false"
defaultParams["SourceIp"]="192.168.178.15"
defaultParams["SourceHostname"]="Pandora.fritz.box"
defaultParams["UtcTime"]="2018-10-11 14:42:15.297"
defaultParams["ProcessGuid"]="{8D54906B-6142-5BBF-0000-00102BEB0F00}"
defaultParams["ProcessId"]="1424"
defaultParams["Image"]="C:\\Users\\Admin\\Download\\adobeUpdate.exe"
defaultParams["User"]="Pandora\\Admin"

defaultParams["Protocol"]="udp"
defaultParams["SourcePort"]="61187"
defaultParams["SourcePortName"]=""
defaultParams["DestinationIsIpv6"]="false"
defaultParams["DestinationIp"]="178.33.163.255"
defaultParams["DestinationHostname"]=""
defaultParams["DestinationPort"]="6893"

def getNetworkParams():
    return defaultParams



def getGithubParams():
    explicitParams = {}
    explicitParams["Protocol"]="tcp"
    explicitParams["SourcePort"]="61187"
    explicitParams["SourcePortName"]=""
    explicitParams["DestinationIsIpv6"]="false"
    explicitParams["DestinationIp"]="140.82.118.3"
    explicitParams["DestinationHostname"]="github.com"
    explicitParams["Image"]="C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe"
    explicitParams["DestinationPort"]="443"
    return explicitParams

def getPastebinParams():
    explicitParams = {}
    explicitParams["Protocol"]="udp"
    explicitParams["SourcePort"]="61185"
    explicitParams["SourcePortName"]=""
    explicitParams["DestinationIsIpv6"]="false"
    explicitParams["DestinationIp"]="104.20.209.21"
    explicitParams["DestinationHostname"]="cre.pastebin.com"
    explicitParams["Image"]="C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe"
    explicitParams["DestinationPort"]="443"
    return explicitParams

def getPShellConnParams():
    explicitParams = {}
    explicitParams["Protocol"]="tcp"
    explicitParams["SourcePort"]="61187"
    explicitParams["SourcePortName"]=""
    explicitParams["DestinationIsIpv6"]="false"
    explicitParams["DestinationIp"]="178.33.163.255"
    explicitParams["DestinationHostname"]="rz.mycrack.to"
    explicitParams["DestinationPort"]="80"
    explicitParams["Image"]="C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe"
    return explicitParams

def getSuspCommParams():
    explicitParams = {}
    explicitParams["Protocol"]="udp"
    explicitParams["SourcePort"]="61187"
    explicitParams["SourcePortName"]=""
    explicitParams["DestinationIsIpv6"]="false"
    explicitParams["DestinationIp"]="178.33.163.255"
    explicitParams["DestinationHostname"]=""
    explicitParams["DestinationPort"]="4443"
    return explicitParams