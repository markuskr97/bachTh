defaultParams = {}
defaultParams["UtcTime"]="2018-10-11 14:44:56.452"
defaultParams["ProcessGuid"]="{8D54906B-61E8-5BBF-0000-0010234E1200}"
defaultParams["ProcessId"]="2948"
defaultParams["Image"]="C:\\users\\admin\\Downloads\\adobeUpdate.exe"
defaultParams["FileVersion"]="11.00.9600.16428 (winblue_gdr.131013-1700)"
defaultParams["Description"]="Adobe (R) Player for Flash"
defaultParams["Product"]="Adobe Flash Player"
defaultParams["Company"]="Adobe"
defaultParams["CommandLine"]="C:\\users\\admin\\Downloads\\adobeUpdate.exe"
defaultParams["CurrentDirectory"]="c:\\users\\admin\\Dowloads\\"
defaultParams["User"]="Pandora\\Admin"
defaultParams["LogonGuid"]="{8D54906B-6039-5BBF-0000-002099080300}"
defaultParams["LogonId"]="0x30899"
defaultParams["TerminalSessionId"]="1"
defaultParams["IntegrityLevel"]="Medium"
defaultParams["Hashes"]="MD5=ABDFC692D9FE43E2BA8FE6CB5A8CB95A,SHA256=949485BA939953642714AE6831D7DCB261691CAC7CBB8C1A9220333801F60820"
defaultParams["ParentProcessGuid"]="{8D54906B-6142-5BBF-0000-00102BEB0F00}"
defaultParams["ParentProcessId"]="1424"
defaultParams["ParentImage"]="C:\\Windows\\explorer.exe"
defaultParams["ParentCommandLine"]="C:\\Windows\\explorer.exe"

def getProcessParams():
    return defaultParams



def getCMDParams():
    explicitParams = {}
    explicitParams["Image"] = "C:\\Windows\\System32\\cmdkey.exe"
    explicitParams["CommandLine"] = "cmdkey /list"
    return explicitParams

def getHideParams():
    explicitParams = {}
    explicitParams["Image"] = "C:\\Windows\\System32\\attrib.exe"
    explicitParams["CommandLine"] = "attrib +h file"
    explicitParams["ProcessGuid"] = "{8D54906B-61E8-5BBF-1111-0010234E1200}"
    explicitParams["ProcessId"] = "2848"
    return explicitParams

def getNetDomainParams():
    explicitParams = {}
    explicitParams["Image"] = "C:\\Windows\\System32\\net.exe"
    explicitParams["ProcessGuid"] = "{8D54906B-61E8-5BBF-0000-0010234E1202}"
    explicitParams["ProcessId"] = "2952"
    explicitParams["CommandLine"] = "net group \"domain admins\" /domain" 
    return explicitParams

def getNetViewParams():
    explicitParams = {}
    explicitParams["Image"] = "C:\\Windows\\System32\\net.exe"
    explicitParams["ProcessGuid"] = "{8D54906B-61E8-5BBF-0000-0010234E1201}"
    explicitParams["ProcessId"] = "2951"
    explicitParams["CommandLine"] = "net view" 
    return explicitParams

def getOfficeShellParams():
    explicitParams = {}
    explicitParams["Image"] = "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe"
    explicitParams["ParentImage"] = "C:\\Program Files\\Microsoft Office\\WINWORD.exe"
    explicitParams["ProcessGuid"] = "{8D54906B-61E8-5BBF-0000-0010234E1200}"
    explicitParams["ProcessId"] = "2950"
    explicitParams["CommandLine"] = "powershell Invoke-WebRequest 10.10.4.147" 
    explicitParams["CurrentDirectory"] = "c:\\users\\markus\\"
    explicitParams["ParentProcessGuid"] = "{8D54906B-6142-5B42-0000-00102BEB0F00}"
    explicitParams["ParentProcessId"]="1424"
    explicitParams["ParentCommandLine"] = "C:\\Program Files\\Microsoft Office\\WINWORD.exe"
    return explicitParams