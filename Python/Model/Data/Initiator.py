from actions.CommonAction import CommonAction
from actions.ImplicitAction import ImplicitAction

from actionDefinition.fileCreation.ActionConfig import getAutoStartParams, getFileDefaultParams, getPShellCommandParams, getQuarkspwParams
from actionDefinition.processCreation.ActionConfig import getCMDParams, getHideParams, getNetDomainParams, getNetViewParams, getOfficeShellParams, getProcessParams
from actionDefinition.networkConnection.ActionConfig import getGithubParams, getNetworkParams, getPastebinParams, getPShellConnParams, getSuspCommParams
from actionDefinition.registryEvent.ActionConfig import getIfeoParams, getRegistryParams, getRunOnceParams, getRunParams

class Initiator:

    def __init__(self):
        self.commonActions = []
        self.implicitActions = {}

    
    def addCommonAction(self,name, eventType, implicActions, paramList):
        comAction = CommonAction(name, eventType, implicActions, paramList)
        self.commonActions.append(comAction)

    def addImplicAction(self,name, params, successReward, failReward, repeatable, eventID):
        implicAction = ImplicitAction(name, params, successReward, failReward, repeatable)
        try:
            self.implicitActions[eventID].append(implicAction)
        except:
            self.implicitActions[eventID] = []
            self.implicitActions[eventID].append(implicAction)
    
    def getCommonActions(self):
        return self.commonActions

    @staticmethod
    def basicInit():
        init = Initiator() 
        #Process Creation
        init.addImplicAction("cmdkey", getCMDParams(), 4, -4, True, "1")
        init.addImplicAction("hideFile", getHideParams(), 2, -2, True, "1")
        init.addImplicAction("netDomain", getNetDomainParams(), 3, -3, True, "1")
        init.addImplicAction("netView", getNetViewParams(), 3, -3, True, "1")
        init.addImplicAction("officeShell", getOfficeShellParams(), 20, -5, True, "1")
        init.addCommonAction("New Process Created", "1", init.implicitActions["1"], getProcessParams())

        #FileCreation
        init.addImplicAction("FileInAutostart", getAutoStartParams(), 10, -5, True, "11")
        init.addImplicAction("malShellCommandFiles", getPShellCommandParams(), 4, -2, True, "11")
        init.addImplicAction("quarkspw", getQuarkspwParams(), 2, -2, True, "11")
        init.addCommonAction("New File", "11", init.implicitActions["11"], getFileDefaultParams())

        #NetworkConnection
        init.addImplicAction("connectGithub", getGithubParams(), 7, -4, True, "3")
        init.addImplicAction("connectPastebin", getPastebinParams(), 7, -4, True, "3")
        init.addImplicAction("powershell", getPShellConnParams(), 11, -5, True, "3")
        init.addImplicAction("suspComm", getSuspCommParams(), 9, -4, True, "3")
        init.addCommonAction("Network", "3", init.implicitActions["3"], getNetworkParams())


        #Registry Event
        init.addImplicAction("ifeo", getIfeoParams(), 18, -7, True, "13")
        init.addImplicAction("runOnce", getRunOnceParams(), 15, -7, True, "13")
        init.addImplicAction("run", getRunParams(), 15, -7, True, "13")
        init.addCommonAction("Registry", "13", init.implicitActions["13"], getRegistryParams())

        return init.getCommonActions()







