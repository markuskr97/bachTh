import random
import json
import logging

from copy import deepcopy

class CommonAction:

    def __init__(self, name, eventType, implicActions, paramList):
        self.name = name
        self.eventType = eventType
        self.defaultParams = paramList
        self.implicitActions = (implicActions)
        self.status = [0] * len(self.implicitActions)
        #QLearning status
        self.lastAction = 0
        self.qval = 0
        self.openQTable()
        self.updateQtable()
        #QLearning Params
        self.epsilon = 9
        self.gamma = 0.95
        self.alpha = 0.3

    def updateQtable(self):
        logging.info("Qtable update")
        try:
            (self.qTable[str(self.status) + '|'+ str(0)])
            logging.info("Qtable is up to data")
        except:
            for i in range(0, len(self.implicitActions)):
                self.qTable[str(self.status) + '|'+ str(i)]=0
            logging.info("QTable was extended")
    
    def act(self):
        logging.info("Acting in " + str(self.status))
        allowedActions = self.getPossibleActions()
        chosenAction = None
        oldStatus = deepcopy(self.status)

        #Choose action
        randNum = random.randint(0,10)
        if(randNum > self.epsilon):
            chosenImplicitAction = 0
            biggestQvalue = 0
            for action in allowedActions:
                qValue = self.getQValue(self.status, action)
                if(biggestQvalue < qValue):
                    biggestQvalue = qValue
                    chosenImplicitAction = action
            if biggestQvalue == 0:
                chosenAction = allowedActions[random.randint(0, len(allowedActions)-1)]
            else:
                chosenAction = chosenImplicitAction
        else:
            chosenAction = allowedActions[random.randint(0, len(allowedActions)-1)]

        self.lastAction = self.implicitActions.index(chosenAction)

        logging.info(str(self.lastAction) + " was chosen")

        terminal = False
        #Recevie rewardimplicActions
        print(self.lastAction)
        reward = self.implicitActions[self.lastAction].act(self.defaultParams, self.eventType)
        #Update Status
        self.status[self.lastAction] += 1
        if(reward < 0):
            terminal = True
        #Extend qtable
        self.updateQtable()
        #Update qvalues
        self.updateQValue(oldStatus, self.lastAction, reward, self.status, terminal)
        #Dump qValues
        self.storeQTable()
        if(terminal):
            logging.info("State reseted, terminal state entered")
            self.status = [0]*len(self.implicitActions)
        #Update best qValue
        self.qval = self.getBestQValue(oldStatus)
        return self.qval,terminal

    def updateQValue(self, state, action, reward, nextState, terminal):
        logging.info("Qvalue update: " + str(state) + ", " + str(action) + ", " + str(reward) + ", " + str(nextState))
        oldValue = self.getQValue(state, action)
        if not terminal:
            newValue = reward + (self.gamma * self.getBestQValue(nextState))
        else:
            newValue = reward
        finalValue = oldValue + self.alpha * (newValue - oldValue)
        self.setQValue(state,action,finalValue)

    def getQValue(self, state, action):
        try:
            qvalue = self.qTable[str(state)+'|'+str(action)]
            logging.info("Fetching qvalue successfull")
            return qvalue
        except:
            logging.info("Fetching qvalue failed")
            return 0
    
    def setQValue(self, state, action, newQValue):
        logging.info("New qvalue set")
        self.qTable[str(state)+'|'+str(action)] = newQValue

    def getBestQValue(self, state):
        biggestQvalue = 0
        for actionIndex in range(0,len(self.implicitActions)):
            qValue = self.getQValue(state,actionIndex)
            if(biggestQvalue < qValue):
                biggestQvalue = qValue
        logging.info("Best qvalue fetched: " + str(biggestQvalue))
        return biggestQvalue

    def getPossibleActions(self):
        logging.info("fetching possible actions")
        allowedActions = []
        for action in self.implicitActions:
            if (action.check()):
                allowedActions.append(action)
        return allowedActions
    
    def storeQTable(self):
        logging.info("Store qtable")
        with open(self.eventType + '.qtable.json','w') as fp:
            json.dump(self.qTable, fp)

    def openQTable(self):
        logging.info("Opten qtable")
        try:
            with open(self.eventType + '.qtable.json','r') as fp:
                self.qTable=json.load(fp)
        except:
            self.qTable = {}
    
    def checkActions(self, implicActions):
        logging.info("Check qvalues")
        allowedActions = []
        for action in implicActions:
            paramList = action.getParamKeys()
            for param in paramList:
                if param in self.defaultParams:
                    allowedActions.append(action)
        return allowedActions
    
    def getQTable(self):
        return self.qTable

    def getEventType(self):
        return self.eventType

