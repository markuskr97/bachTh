import xml.etree.ElementTree as ET
import logging
import datetime
class Parser:

    @staticmethod
    def getString(eventID,params):
        logging.info("Parsing log")
        result = (ET.tostring(Parser.buildTree(eventID,params)))
        result = result.replace(" /", "/")
        result = result.replace("\"","'")
        result = result.replace("##","")
        return result

    @staticmethod
    def buildTree(eventID,values):
        event = ET.Element("Event", xmlns="http://schemas.microsoft.com/win/2004/08/events/event")
        Parser.buildSystemTree(event,eventID)
        Parser.buildEventTree(values, event)
        return event
    
    @staticmethod
    def buildSystemTree(event,eventID):
        system = ET.SubElement(event, "System")
        #Subelemts in System
        ET.SubElement(system, "Provider", Name="Microsoft-Windows-Sysmon", Guid="{5770385F-C22A-43E0-BF4C-06F5698FFBD9}")
        ET.SubElement(system, "EventID").text = eventID
        ET.SubElement(system, "Version").text = "5"
        ET.SubElement(system, "Level").text = "4"
        ET.SubElement(system, "Task").text = eventID
        ET.SubElement(system, "Opcode").text = "0"
        ET.SubElement(system, "Keywords").text = "0x8000000000000000"
        ET.SubElement(system, "TimeCreated", SystemTime = str(datetime.datetime.now()))
        ET.SubElement(system, "EventRecordID").text = "3969"
        ET.SubElement(system, "Correlation")
        ET.SubElement(system, "Execution", ProcessID = "1772", ThreadID = "1584")
        ET.SubElement(system, "Channel").text = "Microsoft-Windows-Sysmon/Operational"
        ET.SubElement(system, "Computer").text = "Pandora"
        ET.SubElement(system, "Security", UserID = "S-1-5-18")

    @staticmethod
    def buildEventTree(parameterList, event):
        eventData = ET.SubElement(event, "EventData")
        ET.SubElement(eventData, "Data", Name = "RuleName").text="##"
        for key in parameterList:
            #Subelements in EventData
            ET.SubElement(eventData, "Data", Name = key).text = parameterList[key]
