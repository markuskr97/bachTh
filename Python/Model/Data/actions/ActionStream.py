import logging
class ActionStream:
    
    def __init__(self):
        self.actionLog = {}
    
    def writeToLog(self, process, sysmonActionID):
        try:
            self.actionLog[process]['events'].append(sysmonActionID)
        except:
            self.actionLog[process] = {}
            self.actionLog[process]['events'] = []
            self.actionLog[process]['events'].append(sysmonActionID)
    
    def getEvents(self,process):
        try:
            return self.actionLog[process]['events']
        except:
            return []

    def check(self,requirements):
        checkResult = False
        for process in requirements:
            if set(requirements[process]).issubset(set(self.getEvents(process))):
                checkResult = True
            else:
                return False
        return checkResult