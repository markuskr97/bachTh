import logging
from splunk.SplunkAdapter import SplunkAdapter
from logparser.Parser import Parser
from copy import deepcopy

class ImplicitAction:

    def __init__(self, name, params, successReward, failReward, repeatable):
        self.name = name
        self.params = params
        self.succes = successReward
        self.fail = failReward
        self.repeat = repeatable
        self.counter = 0
    
    def check(self):
        if (not self.repeat) and (self.counter > 0):
            logging.warning("Implicit Action:" + self.name + ": Was already used.")
            return False
        else:
            return True

    def act(self,defaultParams, eventId):
        print(self.name)
        logParameters = self.combineParams(defaultParams, self.params)
        self.counter += 1
        logging.info(self.name + " executed")
        if SplunkAdapter.getSplunkFeedback(Parser.getString(eventId,logParameters)):#TODO import Splunk and Parser
            logging.info(self.name + ": Received negative feedback")
            print("failed")
            return self.fail
        else:
            print("Successful")
            logging.info(self.name + ": Received positive feedback")
            return self.succes
    
    def combineParams(self, default, implicValues):
        logging.info("Combining params")
        logValues = deepcopy(default)
        for key in implicValues:
            logValues[key] = implicValues[key]
        return logValues