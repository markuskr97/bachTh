import splunklib.client as client
import time
import logging

class SplunkAdapter:
    def __init__(self, host="192.168.178.12", port="8089", username="admin", password="12345678"):
        logging.info("Initiate Splunk")
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.service = None
    

    def connectToSplunk(self):
        logging.info("Connectiong to splunk")
        service = client.connect(
            host = self.host,
            port = self.port,
            username = self.username,
            password = self.password
        )

        self.service = service

    def pushToSplunk (self, index = "devindex", ev="empty event"):
        logging.info("Push to splunk")
        service = self.service
        target = service.indexes[index]
        target.submit(event=ev, host="Pandora", sourcetype="XmlWinEventLog:Microsoft-Windows-Sysmon/Operational", source="WinEventLog:Microsoft-Windows-Sysmon/Operational")

    def checkForAlert(self):
        logging.info("Check for fired alerts")
        alerts = []
        alerts = self.service.fired_alerts
        for alert in alerts:
            print(alert.name)
        if len(alerts) > 1 :
            return True
        else:
            return False

    
    def resetAlerts(self):
        print("... reseting ...")

    @staticmethod
    def getSplunkFeedback(eventString):
        #splunk = SplunkAdapter()
        #splunk.connectToSplunk()
        #splunk.pushToSplunk(ev = eventString)
        #time.sleep(60)
        #detected = splunk.checkForAlert()
        #splunk.resetAlerts()
        if("attrib.exe" in eventString):
            return False
        elif("Debugger" in eventString):
            return False
        elif ("WINWORD" in eventString):
            return False
        else:
            return True
