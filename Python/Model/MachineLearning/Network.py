from keras.initializers import normal, identity
from keras.models import Sequential
from keras.models import load_model, model_from_json
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import SGD , Adam
from keras.callbacks import TensorBoard

from time import time

import numpy as np

import json

class Network:

    def __init__(self):
        self.qTable= {}
        self.neuralNet = self.createModel()
        self.experienceDic = {}
        self.xValues = []
        self.yValues = []
        self.openList()
        self.initData()
        self.createDatasets()

    def createModel(self):
        model = Sequential()
        model.add(Dense(30, input_shape=(4,), activation='relu'))
        model.add(Dense(70, activation='relu'))
        model.add(Dense(30, activation='relu'))
        model.add(Dense(30, activation='relu'))
        model.add(Dense(4, activation='linear'))
        model.compile(loss='mse', optimizer=Adam(lr=0.01))
        return model

    def initData(self):
        for key in self.qTable:
            qValue = self.qTable[key]
            state,action = Network.parseString(key)
            state = str(state)
            state = state.replace("u", "")
            state = state.replace("'", "")
            try:
                self.experienceDic[state][action] = qValue
            except:
                self.experienceDic[state] = [0] * 4
                self.experienceDic[state][action] = qValue

    @staticmethod
    def parseString(keyString):
        listString, actionString = keyString.split("|")
        action = int(actionString)
        listString = listString.replace("[", "")
        listString = listString.replace("]", "")
        listString = listString.replace(",", "")
        liste = listString.split()
        return liste, action

    def createDatasets(self):
        for key in self.experienceDic:
            state = np.array(self.createList(key)).reshape(1,4)
            qvalues = self.experienceDic[key]
            qvalues = np.array(qvalues).reshape(1,4)
            self.xValues.append(state)
            self.yValues.append(qvalues)
    
    def createList(self, listString):
        listString = listString.replace("[", "")
        listString = listString.replace("]", "")
        listString = listString.replace(",", "")
        liste = listString.split()
        return liste

    def openList(self):
        with open('file.qtable.json','r') as fp:
            self.qTable=json.load(fp)
            print(len(self.qTable))
    
    def trainNetwork(self):
        memoryX = np.array(self.xValues).reshape(len(self.xValues),4)
        memoryY = np.array(self.yValues).reshape(len(self.yValues),4)

        tensorboard = TensorBoard(log_dir="logs/{}".format(time()))

        self.neuralNet.fit(memoryX, memoryY, epochs=50, verbose=1, validation_split=0.2, callbacks=[tensorboard])
    
    def predict(self, xValues):
        return self.neuralNet.predict(xValues)

if __name__ == "__main__":
    net = Network()
    net.trainNetwork()
        
    