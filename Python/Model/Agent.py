import json
import random
import logging

from Data.Initiator import Initiator
from Data.analytics.qtableRenderer import renderTable
from copy import deepcopy

class Agent:

    def __init__(self):
        self.commonActions = Initiator.basicInit()
        self.status = [0]*len(self.commonActions)
        self.lastAction = 0
        self.openQTable()
        self.updateQtable

        self.epsilon = 9
        self.gamma = 0.9
        self.alpha = 0.3

        self.episodes = 0
        self.moves = 0

    def updateQtable(self):
        logging.info("1. updating qtable")
        try:
            (self.qTable[str(self.status) + '|'+ str(0)])
        except:
            for i in range(0, len(self.commonActions)):
                self.qTable[str(self.status) + '|'+ str(i)]=0

    def run(self, numberEpisodes = 10000):
        while(self.episodes < numberEpisodes):
            self.act()
        self.createReport()
    
    def act(self):
        chosenAction = None
        oldStatus = deepcopy(self.status)

        #Choose action
        randNum = random.randint(0,10)
        if(randNum > self.epsilon):
            biggestQvalue,chosenImplicitAction = self.getBestSituation(oldStatus)
            if biggestQvalue == 0:
                chosenAction = random.randint(0, len(self.commonActions)-1)
            else:
                chosenAction = chosenImplicitAction
        else:
            chosenAction = random.randint(0, len(self.commonActions)-1)

        self.lastAction = chosenAction

        logging.info(str(self.lastAction) + "was chosen")

        #Update Status
        self.status[self.lastAction] += 1
        self.moves += 1
        #Recevie rewardimplicActions
        bestExplicitQ,terminal = self.commonActions[self.lastAction].act()
        #Extend qtable
        self.updateQtable()
        #Update qvalues
        self.setQValue(oldStatus, self.lastAction, bestExplicitQ)
        #Dump qValues
        self.storeQTable()
        if(terminal or self.moves > 200):
            logging.info("1. terminal state entered, reseting status")
            self.episodes += 1
            self.moves = 0
            self.status = [0]*len(self.commonActions)
        
    def createReport(self):
        renderTable(self.qTable, "Agent.txt")

        for common in self.commonActions:
            renderTable(common.getQTable(), common.getEventType() + ".txt")

    def getQValue(self, state, action):
        try:
            qvalue = self.qTable[str(state)+'|'+str(action)]
            logging.info("1. Fetching qvalue successfull")
            return qvalue
        except:
            logging.info("1. Fetching qvalue failed")
            return 0
    
    def setQValue(self, state, action, newQValue):
        logging.info("1. Qvalue set")
        self.qTable[str(state)+'|'+str(action)] = newQValue

    def getBestSituation(self, state):
        biggestQvalue = 0
        chosenActionIndex = 0
        for actionIndex in range(0,len(self.commonActions)):
            qValue = self.getQValue(state,actionIndex)
            if(biggestQvalue < qValue):
                biggestQvalue = qValue
                chosenActionIndex = actionIndex
        return (biggestQvalue, chosenActionIndex)

    def storeQTable(self):
        logging.info("1. storing qtable")
        with open('agent.qtable.json','w') as fp:
            json.dump(self.qTable, fp)

    def openQTable(self):
        logging.info("1. opening qtable")
        try:
            with open('agent.qtable.json','r') as fp:
                self.qTable=json.load(fp)
        except:
            self.qTable = {}

if __name__ == "__main__":
    logging.basicConfig(filename='model.log', level=logging.DEBUG)
    agent = Agent()
    agent.run()
    