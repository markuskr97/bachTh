import json
import random
import numpy as np
from keras.initializers import normal, identity
from keras.models import Sequential
from keras.models import load_model, model_from_json
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import SGD , Adam
from collections import deque
from copy import deepcopy


class Bot(object):
    """
    The Bot class that applies the Qlearning logic to Flappy bird game
    After every iteration (iteration = 1 game that ends with the bird dying) updates Q values
    After every DUMPING_N iterations, dumps the Q values to the local JSON file
    """

    def __init__(self):
        self.gameCNT = 0  # Game count of current run, incremented after every death
        self.DUMPING_N = 5  # Number of iterations to dump Q values to JSON after
        self.discount = 1.0
        self.memory = deque(maxlen=1000)
        self.r = {0: 0.01, 1: -1000, 2: 10}  # Reward function
        self.last_state = [420,240,0]
        self.last_action = 0
        self.moves = []
        self.experience = []
        self.batchSize = 32
        self.gamma = 0.95
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.learning_rate = 0.001
        self.tau = 20
        self.action_size = 2
        self.filename = "model.h5"
        self.modelDQN, self.modelTarget = self.load_model()


    def load_model(self):

        try:
            model1 = load_model(self.filename)
            model2 = load_model(self.filename)

            # Model reconstruction from JSON file
            with open('model_architecture.json', 'r') as f:
                model1 = model_from_json(f.read())

            # Load weights into model
                model1.load_weights('model_weights.h5')

            # Model reconstruction from JSON file
            with open('model_architecture.json', 'r') as f:
                model2 = model_from_json(f.read())

            # Load weights into model
                model2.load_weights('model_weights.h5')

        except:
            # Neural Net for Deep-Q learning Model
            model1 = Sequential()
            model1.add(Dense(256, input_shape=(3,), activation='relu'))
            model1.add(Dense(256, activation='relu'))
            model1.add(Dense(self.action_size, activation='linear'))
            model1.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))

            model2 = Sequential()
            model2.add(Dense(256, input_shape=(3,), activation='relu'))
            model2.add(Dense(256, activation='relu'))
            model2.add(Dense(self.action_size, activation='linear'))
            model2.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))

            model1, model2 = self.update_model(model1, model2)

        return model1,model2

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, xdif, ydif, vel):
        """
        Chooses the best action with respect to the current state - Chooses 0 (don't flap) to tie-break
        """
        state = np.array([xdif, ydif, vel]).reshape(1,3)

        self.moves.append(
            (self.last_state, self.last_action, state)
        )  # Add the experience to the history

        self.last_state = state  # Update the last_state with the current state

        if np.random.rand() <= self.epsilon:
            self.last_action = random.randrange(self.action_size)
        else:
            act_values = self.modelDQN.predict(state)
            self.last_action = np.argmax(act_values[0])  # returns action

        return self.last_action

    def update_scores(self, crashed, inTube):
        """
        Update qvalues via iterating over experiences
        """
        self.gameCNT += 1  # increase game count
        history = list(reversed(self.moves))
        start_state = history[0][0]
        action = history[0][1]
        next_state = history[0][2]
        reward = self.r[0]

        if(crashed[0]):
            reward = self.r[1]
        elif(inTube):
            reward = self.r[2]
        
    
        self.remember(start_state, action, reward, next_state, crashed)
        
        if self.gameCNT % 350 == 0:
            if(len(self.memory) > self.batchSize):
                self.replay(self.batchSize) 
        
        if self.gameCNT % 500 == 0:
            self.modelDQN, self.modelTarget = self.update_model(self.modelDQN, self.modelTarget)

        self.moves = []  # clear history after updating strategies


    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
              target = reward + self.gamma * \
                       np.amax(self.modelDQN.predict(next_state)[0])
            print(state, action, reward)
            target_f = self.modelTarget.predict(state)
            target_f[0][action] = target
            self.modelDQN.fit(state, target_f, epochs=1, verbose=0)

        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def update_model(self, model1, model2):
        # Save the weights
        model1.save_weights('model_weights.h5')

        # Save the model architecture
        with open('model_architecture.json', 'w') as f:
            f.write(model1.to_json())

        # Load weights into the new model
        model2.load_weights('model_weights.h5')

        print("Saved model to disk")

        return model1, model2