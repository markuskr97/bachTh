import json
import random

from copy import deepcopy


class Bot(object):
    """
    The Bot class that applies the Qlearning logic to Flappy bird game
    After every iteration (iteration = 1 game that ends with the bird dying) updates Q values
    After every DUMPING_N iterations, dumps the Q values to the local JSON file
    """

    def __init__(self):
        self.gameCNT = 0  # Game count of current run, incremented after every death
        self.DUMPING_N = 5  # Number of iterations to dump Q values to JSON after
        self.discount = 1.0
        self.r = {0: 0, 1: -1000, 2: 1000, 3: -3000, 4: -1500, 5: -2500}  # Reward function
        self.lr = 0.7
        self.load_qvalues()
        self.last_state = "420_240_0"
        self.last_action = 0
        self.moves = []
        self.experience = []
        self.batchSize = 32
        self.tau = 20

    def load_qvalues(self):
        """
        Load q values from a JSON file
        """
        self.qvalues = {}
        self.tqValues = {}
        try:
            fil = open("qvalues.json", "r")
        except IOError:
            return
        self.qvalues = json.load(fil)
        self.tqValues = deepcopy(self.qvalues)
        fil.close()

    def act(self, xdif, ydif, vel):
        """
        Chooses the best action with respect to the current state - Chooses 0 (don't flap) to tie-break
        """
        state = self.map_state(xdif, ydif, vel)

        self.moves.append(
            (self.last_state, self.last_action, state)
        )  # Add the experience to the history

        self.last_state = state  # Update the last_state with the current state

        #print("Last Action: ")
        #print(self.last_action)
        #print("................")

        if random.randint(0,99) >= 1:
            if self.qvalues[state][0] > self.qvalues[state][1]:
                self.last_action = 0
                return 0
            if self.qvalues[state][0] < self.qvalues[state][1]:
                self.last_action = 1
                return 1
            else:
                action = random.randint(0,1)
                self.last_action = action
                return action
        else:
            action = random.randint(0,1)
            self.last_action = action
            return action

    def update_scores(self, crashed, inTube):
        """
        Update qvalues via iterating over experiences
        """
        self.gameCNT += 1  # increase game count
        history = list(reversed(self.moves))

        reward = self.r[0]

        ALPHA = 0.3 #learningRate
        GAMMA = 0.99 #discountFactor

        if(crashed[0]):
            self.dump_qvalues()  # Dump q values (if game count % DUMPING_N == 0)
            if(crashed[1]):
                if(crashed[2]):
                    reward = self.r[3]
                else:
                    reward = self.r[4]
            else:
                if(crashed[2]):
                    reward = self.r[5]
                else:
                    reward = self.r[1]
        elif(inTube):
            reward = self.r[2]

        self.experience.append([history[0][0], history[0][1], history[0][2], reward])

        if self.gameCNT % self.batchSize == 0:
            minibatch = random.sample(self.experience, self.batchSize)
            
            for state, action, next_state, rew in minibatch:
                q_old_val = self.qvalues[state][action]
                q_new_val = rew + GAMMA * self.tqValues[next_state][self.qvalues[next_state].index(max(self.qvalues[next_state]))]

                self.qvalues[state][action] = q_old_val + ALPHA * (q_new_val - q_old_val)

            if self.gameCNT % (3 * (self.batchSize)) == 0:
                self.experience = [] 

                #print("New qvalues:")
                #print(self.qvalues[state][action])
                #print("For action")
                #print()
                #print("...........................")
        
        if self.gameCNT % self.tau == 0:
            self.tqValues = deepcopy(self.qvalues)
            #print("copied lists ...")

        self.moves = []  # clear history after updating strategies

    def map_state(self, xdif, ydif, vel):
        """
        Map the (xdif, ydif, vel) to the respective state, with regards to the grids
        The state is a string, "xdif_ydif_vel"

        X -> [-40,-30...120] U [140, 210 ... 420]
        Y -> [-300, -290 ... 160] U [180, 240 ... 420]
        """
        if xdif < 140:
            xdif = int(xdif) - (int(xdif) % 10)
        else:
            xdif = int(xdif) - (int(xdif) % 70)

        if ydif < 180:
            ydif = int(ydif) - (int(ydif) % 10)
        else:
            ydif = int(ydif) - (int(ydif) % 60)

        return str(int(xdif)) + "_" + str(int(ydif)) + "_" + str(vel)

    def dump_qvalues(self):
        """
        Dump the qvalues to the JSON file
        """
        if self.gameCNT % self.DUMPING_N == 0:
            fil = open("qvalues.json", "w")
            json.dump(self.qvalues, fil)
            fil.close()
            print("Q-values updated on local file.")
