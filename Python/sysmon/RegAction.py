import xml.etree.ElementTree as ET
import datetime

class RegAction:

    def __init__(self, target, image, id, guid):
        self.time = str(datetime.datetime.now())
        self.image = image
        self.target = "HKU\\S-1-5-21-2334934307-1691415237-2491240857-1000\\Software\\Microsoft\\Windows\\CurrentVersion\\Run\\" + target
        self.id = id
        self.guid = guid

    
    def createLog(self):
        event = ET.Element("Event")

        system = ET.SubElement(event, "System")
        
        #Subelemts in System
        ET.SubElement(system, "Provider", Name="Microsoft-Sysmon", Guid="{5770385F-C22A-43E0-BF4C-06F5698FFBD9}")
        ET.SubElement(system, "EventID").text = "3"
        ET.SubElement(system, "Version").text = "5"
        ET.SubElement(system, "Level").text = "4"
        ET.SubElement(system, "Task").text = "3"
        ET.SubElement(system, "Opcode").text = "0"
        ET.SubElement(system, "Keywords").text = "0x8000000000000000"
        ET.SubElement(system, "TimeCreated", SystemTime = self.time)
        ET.SubElement(system, "EventRecordID").text = "3969"
        ET.SubElement(system, "Correlation")
        ET.SubElement(system, "Execution", ProcessID = "1772", ThreadID = "1584")
        ET.SubElement(system, "Channel").text = "Microsoft-Windows-Sysmon/Operational"
        ET.SubElement(system, "Computer").text = "Pandora"
        ET.SubElement(system, "Security").text = "S-1-5-18"

        eventData = ET.SubElement(event, "EventData")
        
        #Subelements in EventData
        ET.SubElement(eventData, "Data", Name = "RuleName")
        ET.SubElement(eventData, "Data", Name = "EventType").text = "SetValue"
        ET.SubElement(eventData, "Data", Name = "UtcTime").text = self.time
        ET.SubElement(eventData, "Data", Name = "ProcessGuid").text = self.guid
        ET.SubElement(eventData, "Data", Name = "ProcessId").text = self.id
        ET.SubElement(eventData, "Data", Name = "Image").text = self.image
        ET.SubElement(eventData, "Data", Name = "TargetObject").text = self.target
        ET.SubElement(eventData, "Data", Name = "Details").text = self.image

        return ET.tostring(event)
