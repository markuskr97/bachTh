import xml.etree.ElementTree as ET
import datetime

class Transmission:

    def __init__(self, image, ipSrc, ipDst, srcPort, dstPort, protocol, pId, guid):
        self.src = ipSrc
        self.dest = ipDst
        self.dstPort = dstPort
        self.srcPort = srcPort
        self.prot = protocol
        self.pId = pId
        self.guid = guid
        self.image = image
        self.time = str(datetime.datetime.now())

    def createLog(self):
        event = ET.Element("Event")

        system = ET.SubElement(event, "System")
        
        #Subelemts in System
        ET.SubElement(system, "Provider", Name="Microsoft-Sysmon", Guid="{5770385F-C22A-43E0-BF4C-06F5698FFBD9}")
        ET.SubElement(system, "EventID").text = "3"
        ET.SubElement(system, "Version").text = "5"
        ET.SubElement(system, "Level").text = "4"
        ET.SubElement(system, "Task").text = "3"
        ET.SubElement(system, "Opcode").text = "0"
        ET.SubElement(system, "Keywords").text = "0x8000000000000000"
        ET.SubElement(system, "TimeCreated", SystemTime = self.time)
        ET.SubElement(system, "EventRecordID").text = "3969"
        ET.SubElement(system, "Correlation")
        ET.SubElement(system, "Execution", ProcessID = "1772", ThreadID = "1584")
        ET.SubElement(system, "Channel").text = "Microsoft-Windows-Sysmon/Operational"
        ET.SubElement(system, "Computer").text = "Pandora"
        ET.SubElement(system, "Security").text = "S-1-5-18"

        eventData = ET.SubElement(event, "EventData")
        
        #Subelements in EventData
        ET.SubElement(eventData, "Data", Name = "RuleName")
        ET.SubElement(eventData, "Data", Name = "UtcTime").text = self.time
        ET.SubElement(eventData, "Data", Name = "ProcessGuid").text = self.guid
        ET.SubElement(eventData, "Data", Name = "ProcessId").text = self.pId
        ET.SubElement(eventData, "Data", Name = "Image").text = self.image
        ET.SubElement(eventData, "Data", Name = "User").text = "system"
        ET.SubElement(eventData, "Data", Name = "Protocol").text = self.prot
        ET.SubElement(eventData, "Data", Name = "Initiated").text = "true"
        ET.SubElement(eventData, "Data", Name = "SourceIsIpv6").text = "false"
        ET.SubElement(eventData, "Data", Name = "SourceIp").text = self.src
        ET.SubElement(eventData, "Data", Name = "SourceHostname").text = "Pandora.fritz.box"
        ET.SubElement(eventData, "Data", Name = "SourcePort").text = self.srcPort
        ET.SubElement(eventData, "Data", Name = "SourcePortName")
        ET.SubElement(eventData, "Data", Name = "DestinationIsIpv6").text = "false"
        ET.SubElement(eventData, "Data", Name = "DestinationIp").text = self.dest
        ET.SubElement(eventData, "Data", Name = "DestinationHostname")
        ET.SubElement(eventData, "Data", Name = "DestinationPort").text = self.dstPort
        ET.SubElement(eventData, "Data", Name = "DestinationPortName")
        
        return ET.tostring(event)