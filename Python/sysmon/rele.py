import numpy as np
import random
from SplunkAdapter import SplunkAdapter
from Controller import Controller
import time

class ReLe:
    def __init__(self):
        self.descr = "Class for reinforcement learning"
        self.currentState = "0-0-0-0-0"
        self.state_action_dic = {"0-0-0-0-0":[0,1,2,3,4]}
        self.qtable = {"0-0-0-0-0": [0,0,0,0]}
        self.controller = Controller()
        self.functionDic ={"1":self.controller.createProcess, 
          "2":self.controller.createChangeTime, "3a":self.controller.createTransmissionTCP, "3b":self.controller.createTransmissionUDP,
          "11":self.controller.createWriteFile, "13":self.controller.createRegAction}

    def add_state(self, newState):
      """
      Maps the possible actions to the specific state
      """

      self.state_action_dic[newState] = [0,1,3,4]
      self.qtable[newState] = [0,0,0,0,0]

      stateActions = newState.split('-')

      if(int(stateActions[2]) == 0):
        self.state_action_dic[newState].append(2)

    def choose_mal_action(self, epsilon):
      state_actions = self.qtable[self.currentState]
      maxVal = max(state_actions)
      maxIndex = state_actions.index(maxVal)
      minVal = min(state_actions)
      minIndex = state_actions.index(minVal)
      value = np.random.uniform()
  
      if (value > epsilon):
        if maxVal != minVal:
            action = maxIndex
        else:
          action = random.randint(0, len(state_actions)-1)
      else:
        action = random.randint(0, len(state_actions)-1)
      return action
    
    def get_env_feedback(self, action):
      reward = 0
      newState = 0
      event = self.state_action_dic[self.currentState][action]
      detected = self.getSplunkFeedback(self.functionDic[event])

      if detected == False:
        if event == 0:
          newState = self.currentState|2
          reward = 10
        elif event == 1:
          newState = self.currentState|2
          reward = 20
        elif event == 2:
          newState = self.currentState|4
          reward = 100
        elif event == 3:
          newState = self.currentState|8
          reward = 70
        elif event == 4:
          newState = self.currentState|16
          reward = 70
      else:
        newState = self.currentState
        reward = -1000

      return newState, reward

    def update_Values(self,new_state, reward, action, ALPHA = 0.1, GAMMA = 0.9):
      term = False
      q_old_val = self.qtable[self.currentState][action]
      if new_state != 'terminal':
        q_new_val = reward + GAMMA * max(self.qtable[new_state])
      else:
        q_new_val = reward
        term = True
      self.qtable[self.currentState][action] = q_old_val + ALPHA * (q_new_val - q_old_val)
      return term

    def getSplunkFeedback(self, eventString):
      splunk = SplunkAdapter()
      splunk.connectToSplunk()
      splunk.pushToSplunk(ev = eventString)
      time.sleep(0.5)
      detected = splunk.checkForAlert()
      return detected

    def rl(self):
      self.build_action_state_dic()
      self.build_q_list()
      self.currentState = 0

      terminated = False

      while not terminated:
        nextAction = self.choose_mal_action(0.8)
        nextState, reward = self.get_env_feedback(nextAction)
        terminated = self.update_Values(nextState, reward, nextAction)
        self.currentState = nextState