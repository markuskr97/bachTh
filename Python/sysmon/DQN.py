import numpy as np
import random
from SplunkAdapter import SplunkAdapter
from Controller import Controller
import time
from keras.initializers import normal, identity
from keras.models import Sequential
from keras.models import load_model, model_from_json
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import SGD , Adam
from collections import deque

class DQN:
    def __init__(self):
        self.descr = "Class for reinforcement learning"
        self.noConTCP = 0
        self.noConUDP = 0
        self.noRegAction = 0
        self.noWriteFile = 0
        self.noChangedTime = 0
        self.currentState = np.array([self.noConTCP, self.noConUDP, self.noRegAction, self.noWriteFile, self.noChangedTime]).reshape(1,5)
        self.memory = deque(maxlen=1000)
        self.learning_rate = 0.001
        self.epsilon = 0.9
        self.epsilon_decay = 0.95
        self.epsilon_min = 0.1
        self.gamma = 0.95
        self.controller = Controller()
        self.functionDic ={0:self.controller.createTransmissionTCP, 1:self.controller.createTransmissionUDP,
          2:self.controller.createRegAction, 3:self.controller.createWriteFile, 4:self.controller.createChangeTime}
        self.action_size = len(self.functionDic)
        self.modelDQN, self.modelTarget = self.load_model()

    def load_model(self):

        try:
            # Model reconstruction from JSON file
            with open('model_architecture.json', 'r') as f:
                model1 = model_from_json(f.read())

            # Load weights into model
                model1.load_weights('model_weights.h5')

            # Model reconstruction from JSON file
            with open('model_architecture.json', 'r') as f:
                model2 = model_from_json(f.read())

            # Load weights into model
            model2.load_weights('model_weights.h5')

            model1.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))
            model2.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))

        except:
            # Neural Net for Deep-Q learning Model
            model1 = Sequential()
            model1.add(Dense(30, input_shape=(5,), activation='relu'))
            model1.add(Dense(70, activation='relu'))
            model1.add(Dense(30, activation='relu'))
            model1.add(Dense(self.action_size, activation='linear'))
            model1.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))

            model2 = Sequential()
            model2.add(Dense(30, input_shape=(5,), activation='relu'))
            model2.add(Dense(70, activation='relu'))
            model2.add(Dense(30, activation='relu'))
            model2.add(Dense(self.action_size, activation='linear'))
            model2.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))

            model2 = self.update_model(model1, model2)

        return model1,model2

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size)
        memoryX = []
        memoryY = []
        for state, action, reward, next_state in minibatch:
            done = reward < 0
            target = reward
            if not done:
              target = reward + self.gamma * \
                       np.amax(self.modelDQN.predict(next_state)[0])
            target_f = self.modelTarget.predict(state)
            target_f[0][action] = target
            memoryX.append(state)
            memoryY.append(target_f)
        
        memoryX = np.array(memoryX).reshape(batch_size, 5)
        memoryY = np.array(memoryY).reshape(batch_size, self.action_size)
        self.modelDQN.fit(memoryX, memoryY, epochs=7, verbose=1)

        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def remember(self, state, action, reward, next_state):
        self.memory.append((state, action, reward, next_state))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            self.last_action = random.randrange(self.action_size)
        else:
            act_values = self.modelDQN.predict(state)
            self.last_action = np.argmax(act_values[0])  # returns action

        return self.last_action
    
    def get_env_feedback(self, action):
        reward = 0
        newState = 0
        detected = self.getSplunkFeedback(self.functionDic[action]())

        if detected == False:
            if action == 0:
                self.noConTCP += 1
                reward = 70
            elif action == 1:
                self.noConUDP += 1
                reward = 70
            elif action == 2:
                self.noRegAction += 1
                reward = 100
            elif action == 3:
                self.noWriteFile += 1
                reward = 30
            elif action == 4:
                self.noChangedTime += 1
                reward = 30
        else:
            reward = -1000
            self.resetState()

        newState = np.array([self.noConTCP, self.noConUDP, self.noRegAction, self.noWriteFile, self.noChangedTime]).reshape(1,5)

        return newState, reward

    def resetState(self):
        self.noConTCP = 0
        self.noConUDP = 0
        self.noRegAction = 0
        self.noWriteFile = 0
        self.noChangedTime = 0

    def getSplunkFeedback(self, eventString):
      splunk = SplunkAdapter()
      splunk.connectToSplunk()
      splunk.pushToSplunk(ev = eventString)
      time.sleep(0.5)
      detected = splunk.checkForAlert()
      print(detected)
      return detected

    def update_model(self, model1, model2):
        # Save the weights
        model1.save_weights('model_weights.h5')

        # Save the model architecture
        with open('model_architecture.json', 'w') as f:
            f.write(model1.to_json())

        # Load weights into the new model
        model2.load_weights('model_weights.h5')

        print("Saved model to disk")

        return model2

    def rl(self):

        terminated = False
    
        episodes = 500
        curEpisode = 0
        counter = 0
        batch_size = 32

        while not terminated:
            print (str(counter))
            print(self.currentState)
            action = self.act(self.currentState)
            print(action)
            nextState, reward = self.get_env_feedback(action)
            self.remember(self.currentState, action, reward, nextState)
            self.currentState = nextState
            print(self.currentState)
            print("----------------------------")
            counter += 1
            if reward < 0:
                curEpisode += 1

            if (counter > batch_size):
                self.replay(batch_size)

            if(episodes < curEpisode):
                terminated = True

if __name__ == "__main__":
    model = DQN()
    model.rl()
