import splunklib.client as client
import time

class SplunkAdapter:
    def __init__(self, host="192.168.178.2", port="8089", username="admin", password="12345678"):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.service = None
    

    def connectToSplunk(self):

        service = client.connect(
            host = self.host,
            port = self.port,
            username = self.username,
            password = self.password
        )

        self.service = service

    def pushToSplunk (self, index = "devindex", st = "sysmon", ev="empty event"):
        service = self.service
        target = service.indexes[index]
        target.submit(event=ev, sourcetype=st)

    def checkForAlert(self):
        alerts = []
        alerts = self.service.fired_alerts
        for alert in alerts:
            print(alert.name)
        if len(alerts) > 1 :
            return True
        else:
            return False

    
    def resetAlert(self):
        print("... reseting ...")

    @staticmethod
    def getSplunkFeedback(eventString):
      splunk = SplunkAdapter()
      splunk.connectToSplunk()
      splunk.pushToSplunk(ev = eventString)
      time.sleep(0.5)
      detected = splunk.checkForAlert()
      return detected
