from Process import Process
from RegAction import RegAction
from Transmission import Transmission
from WriteFile import WriteFile
from TimeChange import TimeChange
import random

class Controller:

    def __init__(self):
        self.id = 0
        self.guid = ""
        self.procListNames = []
        self.procList = []
        self.imageName = ""
        self.srcIp = ""
        self.dstIp = ""
        self.srcPort = ""
        self.dstPort = ""
        self.date = ""
        self.targetName = ""
        self.fakeTime = ""



    def createProcess(self):
        self.id = 0
        newProc = Process(self.imageName, "command line", "directory", "parent Image", "parent command line", "description", self.id, self.guid)
        self.procList.append(newProc)
        return newProc.createLog()

    def createTransmissionTCP(self):
        action = Transmission(self.imageName, self.srcIp, self.dstIp, self.srcPort, self.dstPort, "tcp", self.id, self.guid)
        return action.createLog()

    def createTransmissionUDP(self):
        action = Transmission(self.imageName, self.srcIp, self.dstIp, self.srcPort, self.dstPort, "udp", self.id, self.guid)
        return action.createLog()

    def createRegAction(self):
        action = RegAction(self.targetName, self.imageName, self.id, self.guid)
        return action.createLog()
    
    def createWriteFile(self):
        action = WriteFile("targetDestination",self.imageName, self.id, self.guid)
        return action.createLog()

    def createChangeTime(self):
        action = TimeChange(self.fakeTime,"targetPath", self.imageName, self.id, self.guid)
        return action.createLog()