import xml.etree.ElementTree as ET
import datetime

class Process:

    def __init__(self, image, comLine, direct, parImage, parComLine, desc, pId, guid):
        self.time = str(datetime.datetime.now())
        self.image = image
        self.comLine = comLine
        self.direct = direct
        self.parImage = parImage
        self.parComLine = parComLine
        self.desc = desc
        self.pId = pId
        self.guid = guid

    def createLog(self):
        event = ET.Element("Event")

        system = ET.SubElement(event, "System")
        
        #Subelemts in System
        ET.SubElement(system, "Provider", Name="Microsoft-Sysmon", Guid="{5770385F-C22A-43E0-BF4C-06F5698FFBD9}")
        ET.SubElement(system, "EventID").text = "3"
        ET.SubElement(system, "Version").text = "5"
        ET.SubElement(system, "Level").text = "4"
        ET.SubElement(system, "Task").text = "3"
        ET.SubElement(system, "Opcode").text = "0"
        ET.SubElement(system, "Keywords").text = "0x8000000000000000"
        ET.SubElement(system, "TimeCreated", SystemTime = self.time)
        ET.SubElement(system, "EventRecordID").text = "3969"
        ET.SubElement(system, "Correlation")
        ET.SubElement(system, "Execution", ProcessID = "1772", ThreadID = "1584")
        ET.SubElement(system, "Channel").text = "Microsoft-Windows-Sysmon/Operational"
        ET.SubElement(system, "Computer").text = "Pandora"
        ET.SubElement(system, "Security").text = "S-1-5-18"

        eventData = ET.SubElement(event, "EventData")
        
        #Subelements in EventData
        ET.SubElement(eventData, "Data", Name = "RuleName")
        ET.SubElement(eventData, "Data", Name = "UtcTime").text = self.time
        ET.SubElement(eventData, "Data", Name = "ProcessGuid").text = self.guid
        ET.SubElement(eventData, "Data", Name = "ProcessId").text = self.pId
        ET.SubElement(eventData, "Data", Name = "Image").text = self.image
        ET.SubElement(eventData, "Data", Name = "FileVersion").text = "11.00.9600.16428 (winblue_gdr.131013-1700)"
        ET.SubElement(eventData, "Data", Name = "Description").text = "Microsoft (R) HTML Application Host"
        ET.SubElement(eventData, "Data", Name = "Product").text = "Internet Explorer"
        ET.SubElement(eventData, "Data", Name = "Company").text = "Microsoft Corporation"
        ET.SubElement(eventData, "Data", Name = "CommandLine").text = ""
        ET.SubElement(eventData, "Data", Name = "CurrentDirectory").text = ""
        ET.SubElement(eventData, "Data", Name = "User").text = "Pandora\\Markus"
        ET.SubElement(eventData, "Data", Name = "LogonGuid").text = "{8D54906B-6039-5BBF-0000-002099080300}"
        ET.SubElement(eventData, "Data", Name = "LogoId").text = "0x30899"
        ET.SubElement(eventData, "Data", Name = "TerminalSessionId").text = "1"
        ET.SubElement(eventData, "Data", Name = "IntegrityLEvel").text = "Medium"
        ET.SubElement(eventData, "Data", Name = "Hashes").text = "MD5=ABDFC692D9FE43E2BA8FE6CB5A8CB95A,SHA256=949485BA939953642714AE6831D7DCB261691CAC7CBB8C1A9220333801F60820"
        ET.SubElement(eventData, "Data", Name = "ParentProcessGuid").text = ""
        ET.SubElement(eventData, "Data", Name = "ParentProcessId").text = ""
        ET.SubElement(eventData, "Data", Name = "ParentImage").text = ""
        ET.SubElement(eventData, "Data", Name = "ParentCommandLine").text = ""
        
        return ET.tostring(event)