#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<winsock2.h>

int changeFile(){
    
    FILE* fptr;
    int ch;
    fptr = fopen("test1.txt", "r+");

    if (fptr == NULL)
    {
        fprintf(stderr, "cannot open target file %s\n", "test1.txt");
        return 1;
    }
    while ((ch = fgetc(fptr)) != EOF)
    {
        fseek(fptr, -1, SEEK_CUR);
        fputc('p',fptr);
        fseek(fptr, 0, SEEK_CUR);
    }

    fclose(fptr);

	return 0;
}

int sendUDP(){
    struct sockaddr_in si_other;
    int s, i, slen=sizeof(si_other);
 
    if ( (s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        printf("Failed tp create socket");
    }
 
    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(8888);
    si_other.sin_addr.s_addr = inet_addr("192.168.150.20");

     while(1){
        //send the message
        if (sendto(s, "Test", 4 , 0 , (struct sockaddr *) &si_other, slen)==-1)
        {
            printf("Failed to send message ... ");
            return 1;
        }
    }

    return 0;
    
}

int removeFile(){
    int status = remove("test1.txt");
    return status;
}

int main() {
    int res = sendUDP();
    if (res != 0){
        printf("Failed to delete file ...");
    }
    return 0;
}